﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FigureGenerator : IFigureGenerator
{
    public List<List<CellInfo>> figures = new List<List<CellInfo>>();

    public FigureGenerator()
    {
        var L = new List<CellInfo>()
            {
                new CellInfo(0, 0, CellType.Filled),
                new CellInfo(0, 1, CellType.Filled),
                new CellInfo(0, 2, CellType.Filled),
                new CellInfo(1, 2, CellType.Filled),
            };

        var J = new List<CellInfo>()
            {
                new CellInfo(0, 0, CellType.Filled),
                new CellInfo(0, 1, CellType.Filled),
                new CellInfo(0, 2, CellType.Filled),
                new CellInfo(-1, 2, CellType.Filled),
            };

        var I = new List<CellInfo>()
            {
                new CellInfo(0, 0, CellType.Filled),
                new CellInfo(0, 1, CellType.Filled),
                new CellInfo(0, 2, CellType.Filled),
                new CellInfo(0, 3, CellType.Filled),
            };

        var O = new List<CellInfo>()
            {
                new CellInfo(0, 0, CellType.Filled),
                new CellInfo(0, 1, CellType.Filled),
                new CellInfo(1, 0, CellType.Filled),
                new CellInfo(1, 1, CellType.Filled),
            };

        var S = new List<CellInfo>()
            {
                new CellInfo(0, 0, CellType.Filled),
                new CellInfo(1, 0, CellType.Filled),
                new CellInfo(0, 1, CellType.Filled),
                new CellInfo(-1, 1, CellType.Filled),
            };

        var T = new List<CellInfo>()
            {
                new CellInfo(0, 0, CellType.Filled),
                new CellInfo(1, 0, CellType.Filled),
                new CellInfo(2, 0, CellType.Filled),
                new CellInfo(1, 1, CellType.Filled),
            };

        var Z = new List<CellInfo>()
            {
                new CellInfo(0, 0, CellType.Filled),
                new CellInfo(1, 0, CellType.Filled),
                new CellInfo(1, 1, CellType.Filled),
                new CellInfo(2, 1, CellType.Filled),
            };

        figures.Add(L);
        figures.Add(J);
        figures.Add(I);
        figures.Add(O);
        figures.Add(S);
        figures.Add(T);
        figures.Add(Z);
    }

    public List<CellInfo> GetRandomFigure()
    {
        return figures[Random.Range(0, figures.Count)];
    }
}
