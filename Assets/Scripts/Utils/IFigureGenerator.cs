﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IFigureGenerator
{
    List<CellInfo> GetRandomFigure();
}
