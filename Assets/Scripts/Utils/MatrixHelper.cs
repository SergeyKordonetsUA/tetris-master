﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MatrixHelper : IMatrixHelper
{
    private const float cellSize = 100f;
    private const float padding = 1.2f;
     
    public Vector3 GetPosition(Vector2 startCoord, float x, float y)
    {
        return new Vector3(startCoord.x + x * cellSize + cellSize / 2 + padding,
            startCoord.y - y * cellSize - cellSize / 2 + padding);
    }

    public Vector2 GetSizeDelta()
    {
        return new Vector2(cellSize - padding, cellSize - padding);
    }

    public Vector2 GetStart(int width, int height)
    {
        return new Vector2(-(cellSize + padding) * width / 2, (cellSize + padding) * height / 2);
    }
}
