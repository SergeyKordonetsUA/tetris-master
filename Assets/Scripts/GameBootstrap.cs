﻿using strange.extensions.context.impl;
using UnityEngine;

public class GameBootstrap : ContextView
{
    [SerializeField]
    private UIManager uiManager;
    private GameContext context;

    private void Awake()
    {
        context = new GameContext(uiManager, this);
        context.Start();
        context.Launch();
    }
}
