﻿using strange.extensions.mediation.impl;
using UnityEngine;

public class MenuMediator : Mediator
{
    [Inject]
    public MenuView menuView { get; set; }

    [Inject]
    public StartGameSignal StartGameSignal { get; set; }

    public override void OnRegister()
    {
        base.OnRegister();
        menuView.StartGame.AddListener(OnStartGameClicked);
    }

    private void OnStartGameClicked()
    {
        StartGameSignal.Dispatch(menuView);
    }

    public override void OnRemove()
    {
        base.OnRemove();
        menuView.StartGame.RemoveListener(OnStartGameClicked);
    }
}
