﻿using strange.extensions.signal.impl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PlayerFigureView : ViewWithModel<List<CellInfo>>, IPointerDownHandler, IPointerUpHandler
{
    [SerializeField]
    private GameObject cell;

    [SerializeField]
    private Transform center;

    [Inject]
    public IMatrixHelper matrixHelper { get; set; }

    private Transform movable;
    public readonly List<CellView> pool = new List<CellView>();
    private Vector2 centerPosition;

    public Signal DropFigure = new Signal();

    protected override void OnModelChanged(List<CellInfo> model)
    {
        HideAll();

        for (int i = 0; i < model.Count; i++)
        {
            CellView _cell = null;

            if (i > pool.Count - 1)
            {
                _cell = Instantiate(cell).GetComponent<CellView>();
                pool.Add(_cell);
            }
            else
            {
                _cell = pool[i];
            }

            var x = model[i].row;
            var y = model[i].column;

            _cell.Model = new CellInfo(x, y, model[i].cellType);
            _cell.gameObject.SetActive(true);
            _cell.transform.SetParent(transform, false);
            ((RectTransform)_cell.transform).sizeDelta = matrixHelper.GetSizeDelta();
            _cell.transform.localPosition = matrixHelper.GetPosition(Vector2.zero, x, y);
            _cell.name = string.Format("Cell {0}x{1}", x, y);
        }

        centerPosition = FindCenter();

        center.localPosition = matrixHelper.GetPosition(Vector2.zero, centerPosition.x, centerPosition.y);


        for (int i = 0; i < pool.Count; i++)
        {
            pool[i].transform.SetParent(center.transform);
        }

        SetCenterPosition();
    }

    public void SetCenterPosition()
    {
        center.position = transform.position;
    }

    public Vector2 FindCenter()
    {
        int minX = int.MaxValue;
        int minY = int.MaxValue;

        int maxX = int.MinValue;
        int maxY = int.MinValue;

        foreach (var pt in Model)
        {
            if (pt.row < minX)
            {
                minX = pt.row;
            }
            if (pt.column < minY)
            {
                minY = pt.column;
            }

            if (pt.row > maxX)
            {
                maxX = pt.row;
            }
            if (pt.column > maxY)
            {
                maxY = pt.column;
            }
        }

        return new Vector2((float)(minX + maxX) / 2, (float)(minY + maxY) / 2);
    }

    private void Update()
    {
        if (movable == null)
        {
            return;
        }

        foreach (Touch touch in Input.touches)
        {
            var temp = Camera.main.ScreenToWorldPoint(touch.position);
            temp.z = 0;
            movable.position = temp;
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        foreach (Touch touch in Input.touches)
        {
            movable = center.transform;
        }
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        foreach (Touch touch in Input.touches)
        {
            DropFigure.Dispatch();
            movable = null;
        }
    }

    public void HideAll()
    {
        for (int i = 0; i < pool.Count; i++)
        {
            pool[i].gameObject.SetActive(false);
        }
    }
}
