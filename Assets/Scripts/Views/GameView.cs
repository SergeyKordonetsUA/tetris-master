﻿using strange.extensions.mediation.impl;
using UnityEngine;
using UnityEngine.UI;

public class GameView : View
{
    [SerializeField]
    private Text currentScore;

    [SerializeField]
    private Text maxScore;

    public void SetScore(int amount, int maxAmount)
    {
        currentScore.text = amount.ToString();
        maxScore.text = maxAmount.ToString();
    }
}
