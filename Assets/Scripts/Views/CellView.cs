﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CellView : ViewWithModel<CellInfo>
{
    [SerializeField] private Image image;
    [SerializeField] private Color emptyCellColor;
    [SerializeField] private Color filledCellColor;

    protected override void Awake()
    {
        base.Awake();
        if(image == null)
        {
            image = GetComponent<Image>();
        }
    }

    protected override void OnModelChanged(CellInfo model)
    {
        image.color = model.cellType == CellType.Empty ? emptyCellColor : filledCellColor;
    }

    public void ChangeColor(CellType cellType)
    {
        Model.cellType = cellType;
        OnModelChanged(Model);
    }
}
