﻿using strange.extensions.signal.impl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndGameView : MenuView
{
    [SerializeField] private Button endGame;

    public Signal EndGameSignal = new Signal();

    protected override void Start()
    {
        base.Start();
        endGame.onClick.AddListener(EndGameClick);
    }

    private void EndGameClick()
    {
        EndGameSignal.Dispatch();
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();
        endGame.onClick.RemoveListener(EndGameClick);
    }
}
