﻿using strange.extensions.mediation.impl;
using System.Collections.Generic;
using UnityEngine;

public class FieldView : View
{
    [SerializeField]
    private GameObject cell;
    public CellView[,] matrix;

    [Inject]
    public IMatrixHelper matrixHelper { get; set; }

    [Inject]
    public IGameConfig gameConfig { get; set; }

    [Inject]
    public ScoreSignal scoreSignal { get; set; }

    private int width
    {
        get
        {
            return gameConfig.FieldWidth;
        }
    }

    private int height
    {
        get
        {
            return gameConfig.FieldHeight;
        }
    }

    public void Initialize()
    {
        matrix = new CellView[width, height];
        var startCord = matrixHelper.GetStart(width, height);

        for (int i = 0; i < width; ++i)
        {
            for (int j = 0; j < height; ++j)
            {
                var _cell = Instantiate(cell);
                var cellComponent = _cell.GetComponent<CellView>();
                cellComponent.Model = new CellInfo(i, j);
                _cell.layer = LayerMask.NameToLayer("Cell");
                _cell.SetActive(true);
                _cell.transform.SetParent(transform, false);
                ((RectTransform)_cell.transform).sizeDelta = matrixHelper.GetSizeDelta();
                _cell.transform.localPosition = matrixHelper.GetPosition(startCord, i, j);
                _cell.name = string.Format("Cell {0}x{1}", i, j);
                matrix[i, j] = cellComponent;
            }
        }
    }

    public void ClearMatrix()
    {
        for (int i = 0; i < width; ++i)
        {
            for (int j = 0; j < height; ++j)
            {
                matrix[i, j].ChangeColor(CellType.Empty);
            }
        }
    }

    public bool Calculate(List<CellInfo> figure, Vector2 positionToPlace)
    {
        if (CanPlace(figure, positionToPlace))
        {
            for (int i = 0; i < figure.Count; i++)
            {
                var _cell = matrix[(int)(figure[i].row + positionToPlace.x), (int)(figure[i].column + positionToPlace.y)];
                _cell.ChangeColor(figure[i].cellType);
            }
            CheckMatrix();
            return true;
        }
        return false;
    }

    private void CheckMatrix()
    {
        List<int> rowsToRemove = new List<int>() { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
        List<int> columnsToRemove = new List<int> { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };

        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                if (matrix[i, j].Model.cellType == CellType.Empty)
                {
                    rowsToRemove.Remove(i);
                    columnsToRemove.Remove(j);
                }
            }
        }
        RemoveRows(rowsToRemove);
        RemoveColumns(columnsToRemove);
    }

    private void RemoveRows(List<int> rowsNumber)
    {
        for (int i = 0; i < width; i++)
        {
            if (rowsNumber.Contains(i))
            {
                for (int j = 0; j < height; j++)
                {
                    matrix[i, j].ChangeColor(CellType.Empty);
                    scoreSignal.Dispatch(10);
                }
            }
        }
    }

    private void RemoveColumns(List<int> columnsNumber)
    {
        for (int i = 0; i < width; i++)
        {
            if (columnsNumber.Contains(i))
            {
                for (int j = 0; j < height; j++)
                {
                    matrix[j, i].ChangeColor(CellType.Empty);
                    scoreSignal.Dispatch(10);
                }
            }
        }
    }

    public bool CheckAllVariants(List<CellInfo> figure)
    {
        foreach (var item in matrix)
        {
            if (item.Model.cellType == CellType.Empty)
            {
                if (CanPlace(figure, item.Model.Position))
                {
                    return true;
                }
            }
        }
        return false;
    }

    public bool CanPlace(List<CellInfo> figure, Vector2 positionToPlace)
    {
        for (int i = 0; i < figure.Count; i++)
        {
            if (figure[i].row + positionToPlace.x >= width
                ||figure[i].column + positionToPlace.y >= height
                ||figure[i].row + positionToPlace.x < 0
                ||figure[i].column + positionToPlace.y < 0
                ||matrix[(int)(figure[i].row + positionToPlace.x), (int)(figure[i].column + positionToPlace.y)]
                    .Model.cellType != CellType.Empty)
            {
                return false;
            }
        }
        return true;
    }
}
