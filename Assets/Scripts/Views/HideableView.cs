﻿using strange.extensions.mediation.impl;
using UnityEngine;

public class HideableView : View, IHideable
{
    public void Hide(bool isHide = true)
    {
        gameObject.SetActive(!isHide);
    }
}
