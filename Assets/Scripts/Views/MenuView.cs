﻿using strange.extensions.signal.impl;
using UnityEngine;
using UnityEngine.UI;

public class MenuView : HideableView
{
    [SerializeField] private Button startGameButton;
    public Signal StartGame = new Signal();

    protected override void Start()
    {
        base.Start();
        startGameButton.onClick.AddListener(StartGameClick);
    }

    private void StartGameClick()
    {
        StartGame.Dispatch();
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();
        startGameButton.onClick.RemoveListener(StartGameClick);
    }
}
