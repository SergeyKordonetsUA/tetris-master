﻿using strange.extensions.mediation.impl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PlayerFigureMediator : Mediator
{
    [Inject]
    public PlayerFigureView view { get; set; }

    [Inject]
    public UpdatePlayerFigureSignal updatePlayerFigureSignal { get; set; }

    [Inject]
    public DropPlayerFigureSignal dropPlayerFigureSignal { get; set; }

    [Inject]
    public IGameModel gameModel { get; set; }

    [Inject]
    public IFigureGenerator figureGenerator { get; set; }

    public override void OnRegister()
    {
        base.OnRegister();
        updatePlayerFigureSignal.AddListener(UpdateView);
        view.DropFigure.AddListener(OnDropFigure);
    }

    private List<RaycastResult> GetRaycastedObjects(PointerEventData touch)
    {
        List<RaycastResult> objectsHit = new List<RaycastResult>();
        EventSystem.current.RaycastAll(touch, objectsHit);
        return objectsHit;
    }

    private PointerEventData GetTouch()
    {
        var pos = view.pool[0].transform.position;

        PointerEventData touch = new PointerEventData(EventSystem.current)
        {
            position = Camera.main.WorldToScreenPoint(pos)
        };

        return touch;
    }

    private void OnDropFigure()
    {
        var touch = GetTouch();
        var objectsHit = GetRaycastedObjects(touch);

        if (objectsHit.Count > 0)
        {
            foreach (var item in objectsHit)
            {
                var cellComponent = item.gameObject.GetComponent<CellView>();
                if (cellComponent != null && cellComponent.gameObject.layer == LayerMask.NameToLayer("Cell"))
                {
                    dropPlayerFigureSignal.Dispatch(view, cellComponent.Model.Position);
                }
                else
                {
                    view.SetCenterPosition();
                }
            }
        }
        else
        {
            view.SetCenterPosition();
        }
    }

    private void UpdateView()
    {
        var randomFigure = new List<CellInfo>(figureGenerator.GetRandomFigure());
        gameModel.Cells.Add(randomFigure);
        view.Model = randomFigure;
    }

    public override void OnRemove()
    {
        base.OnRemove();
        updatePlayerFigureSignal.RemoveListener(UpdateView);
        view.DropFigure.RemoveListener(OnDropFigure);

    }
}
