﻿using strange.extensions.mediation.impl;

public abstract class ViewWithModel<T> : View where T : class
{
    private T model;

    public T Model
    {
        get
        {
            return model;
        }
        set
        {
            if (model == null || model != value)
            {
                model = value;
                OnModelChanged(model);
            }
        }
    }

    protected abstract void OnModelChanged(T model);

    public void SetModelChanged()
    {
        OnModelChanged(model);
    }
}
