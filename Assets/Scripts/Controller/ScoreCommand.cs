﻿using strange.extensions.command.impl;
using System.Collections.Generic;
using UnityEngine;

public class ScoreCommand : Command
{
    [Inject]
    public IGameModel gameModel { get; set; }

    [Inject]
    public GameView gameView { get; set; }

    [Inject]
    public int amount { get; set; }

    public override void Execute()
    {
        gameModel.Score += amount;

        gameView.SetScore(gameModel.Score, gameModel.MaxScore);
    }
}
