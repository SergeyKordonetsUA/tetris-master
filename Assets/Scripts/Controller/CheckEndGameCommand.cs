﻿using strange.extensions.command.impl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CheckEndGameCommand : Command
{
    [Inject]
    public IGameModel gameModel { get; private set; }

    [Inject]
    public FieldView field { get; private set; }

    [Inject]
    public EndGameSignal endGameSignal { get; private set; }

    protected bool CheckAllFigures()
    {
        for (int i = 0; i < gameModel.Cells.Count; i++)
        {
            if (field.CheckAllVariants(gameModel.Cells[i]))
            {
                return true;
            }
        }

        return false;
    }
}
