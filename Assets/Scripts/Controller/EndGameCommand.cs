﻿using strange.extensions.command.impl;
using UnityEngine;

public class EndGameCommand : Command
{
    [Inject]
    public IGameModel gameModel { get; set; }

    [Inject]
    public GameView gameView { get; set; }

    public override void Execute()
    {
        Debug.Log("Game over!");
        if (gameModel.MaxScore < gameModel.Score)
        {
            gameModel.MaxScore = gameModel.Score;
        }
        gameModel.Score = 0;

        gameView.SetScore(gameModel.Score, gameModel.MaxScore);
    }
}
