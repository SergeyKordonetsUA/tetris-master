﻿using strange.extensions.command.impl;
using UnityEngine;

public class DropFigureCommand : Command
{
    [Inject]
    public PlayerFigureView figureView { get; private set; }

    [Inject]
    public Vector2 position { get; private set; }

    [Inject]
    public FieldView field { get; private set; }

    [Inject]
    public IGameModel gameModel { get; private set; }

    public override void Execute()
    {
        if (field.Calculate(figureView.Model, position))
        {
            figureView.HideAll();
            gameModel.Cells.Remove(figureView.Model);
        }
        else
        {
            figureView.SetCenterPosition();
            Fail();
        }
    }
}
