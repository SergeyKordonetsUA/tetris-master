﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateFiguresCommand : CheckEndGameCommand
{
    [Inject]
    public UpdatePlayerFigureSignal updatePlayerFigureSignal { get; set; }

    public override void Execute()
    {
        updatePlayerFigureSignal.Dispatch();

        if (!CheckAllFigures())
        {
            endGameSignal.Dispatch();
        }
    }
}
