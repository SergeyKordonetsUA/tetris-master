﻿using strange.extensions.command.impl;
using UnityEngine;

public class HideableViews
{
    public IHideable toHide;
    public IHideable toShow;

    public HideableViews(IHideable hide, IHideable show)
    {
        toHide = hide;
        toShow = show;
    }
}

public class InitializeGameCommand : Command
{
    [Inject]
    public HideableViews hidebleViews { get; set; }

    [Inject]
    public IGameModel gameModel { get; set; }

    [Inject]
    public FieldView fieldView { get; set; }

    public override void Execute()
    {
        hidebleViews.toHide.Hide();
        hidebleViews.toShow.Hide(false);

        if (gameModel.IsFirstLaunch)
        {
            Debug.Log("Initialize field");
            gameModel.IsFirstLaunch = false;
            fieldView.Initialize();
        }
    }
}
