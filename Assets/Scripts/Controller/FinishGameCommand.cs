﻿using strange.extensions.command.impl;
using UnityEngine;

public class FinishGameCommand : Command
{

    public override void Execute()
    {
        Debug.Log("Quit");
        Application.Quit();
    }
}
