﻿using strange.extensions.command.impl;
using System.Collections.Generic;
using UnityEngine;

public class StartGameCommand : Command
{
    [Inject]
    public IHideable menuView { get; set; }

    [Inject]
    public IGameModel gameModel { get; set; }

    [Inject]
    public FieldView fieldView { get; set; }

    public override void Execute()
    {
        Debug.Log("Start game");
        menuView.Hide();
        gameModel.Cells = new List<List<CellInfo>>();
        fieldView.ClearMatrix();
    }
}
