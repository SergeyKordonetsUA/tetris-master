﻿public class CheckFieldCommand : CheckEndGameCommand
{
    public override void Execute()
    {
        if (gameModel.Cells.Count != 0)
        {
            Fail();
            if (!CheckAllFigures())
            {
                endGameSignal.Dispatch();
            }
        }
    }
}
