﻿using System.Collections;
using System.Collections.Generic;
using strange.extensions.signal.impl;
using UnityEngine;

public class InitialiseSignal : Signal<HideableViews> { }
public class StartGameSignal : Signal<IHideable> { }
public class UpdatePlayerFigureSignal : Signal { }
public class DropPlayerFigureSignal : Signal<PlayerFigureView, Vector2> { }
public class EndGameSignal : Signal { }
public class FinishGameSignal : Signal { }
public class ScoreSignal : Signal<int> { }
