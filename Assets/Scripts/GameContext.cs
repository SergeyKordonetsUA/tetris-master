﻿using System.Collections;
using strange.extensions.command.api;
using strange.extensions.command.impl;
using strange.extensions.context.api;
using strange.extensions.context.impl;
using UnityEngine;


public class GameContext : MVCSContext
{
    private UIManager uiManager;

    public GameContext(UIManager _uiManager, GameBootstrap contextView)
        : base(contextView, ContextStartupFlags.MANUAL_MAPPING | ContextStartupFlags.MANUAL_LAUNCH)
    {
        uiManager = _uiManager;
    }

    protected override void mapBindings()
    {
        base.mapBindings();
        InjectionBinding();
        CommandsBinding();
        ViewsBinding();
    }

    public override void Launch()
    {
        base.Launch();
        var initSignal = injectionBinder.GetInstance<InitialiseSignal>();
        initSignal.Dispatch(new HideableViews(uiManager.endGameView, uiManager.menuView));
    }

    private void InjectionBinding()
    {
        injectionBinder.Bind<IGameModel>().To<GameModel>().ToSingleton();
        injectionBinder.Bind<IGameConfig>().To<GameConfig>().ToSingleton();
        injectionBinder.Bind<IFigureGenerator>().To<FigureGenerator>().ToSingleton();
        injectionBinder.Bind<IMatrixHelper>().To<MatrixHelper>().ToSingleton();
        injectionBinder.Bind<UpdatePlayerFigureSignal>().To<UpdatePlayerFigureSignal>().ToSingleton();
    }

    private void CommandsBinding()
    {
        commandBinder.Bind<InitialiseSignal>()
                .To<InitializeGameCommand>().Once();


        commandBinder.Bind<StartGameSignal>()
            .To<StartGameCommand>()
            .To<CreateFiguresCommand>()
            .InSequence();

        commandBinder.Bind<DropPlayerFigureSignal>()
            .To<DropFigureCommand>()
            .To<CheckFieldCommand>()
            .To<CreateFiguresCommand>()
            .InSequence();

        commandBinder.Bind<EndGameSignal>()
            .To<EndGameCommand>();

        commandBinder.Bind<FinishGameSignal>()
            .To<FinishGameCommand>();

        commandBinder.Bind<ScoreSignal>()
            .To<ScoreCommand>();
    }

    private void ViewsBinding()
    {
        if (injectionBinder == null || uiManager == null)
        {
            throw new System.Exception();
        }

        mediationBinder.Bind<MenuView>().To<MenuMediator>();
        mediationBinder.Bind<PlayerFigureView>().To<PlayerFigureMediator>();
        injectionBinder.Bind<FieldView>().ToValue(uiManager.fieldView);
        injectionBinder.Bind<GameView>().ToValue(uiManager.gameView);
        mediationBinder.Bind<EndGameView>().To<EndGameMediator>();
    }

    protected override void addCoreComponents()
    {
        base.addCoreComponents();
        injectionBinder.Unbind<ICommandBinder>();
        injectionBinder.Bind<ICommandBinder>().To<SignalCommandBinder>().ToSingleton();
    }
}
