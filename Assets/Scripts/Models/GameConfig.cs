﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameConfig : IGameConfig
{
    private float cellSize = 1;

    public int FieldWidth {
        get
        {
            return 10;
        }
    }

    public int FieldHeight
    {
        get
        {
            return 10;
        }
    }

    public float CellSize
    {
        get
        {
            return cellSize;
        }
        set
        {
            cellSize = value;
        }
    }
}
