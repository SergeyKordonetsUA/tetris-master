﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IGameModel
{
    int Score { get; set; }
    int MaxScore { get; set; }
    bool IsFirstLaunch { get; set; }

    List<List<CellInfo>> Cells { get; set; }
}