﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CellType
{
    Empty,
    Filled
}

public class CellInfo : MonoBehaviour
{
    public int row;
    public int column;
    public CellType cellType;

    public CellInfo(int x, int y, CellType _cellType = CellType.Empty)
    {
        row = x;
        column = y;
        cellType = _cellType;
    }

    public Vector2 Position
    {
        get { return new Vector2(row, column);  }
    }

    public override string ToString()
    {
        return System.String.Format("CellInfo :{0}x{1}", row, column);
    }
}
