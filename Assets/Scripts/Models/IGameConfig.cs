﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IGameConfig
{
    int FieldWidth { get; }
    int FieldHeight { get; }
    float CellSize { get; set; }
}
