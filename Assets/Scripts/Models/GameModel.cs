﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameModel : IGameModel
{
    private bool isFirstLaunch = true;
    public int Score { get; set; }
    public int MaxScore { get; set; }

    public bool IsFirstLaunch
    {
        get { return isFirstLaunch; }
        set { isFirstLaunch = value; }
    }

    public List<List<CellInfo>> Cells { get; set; }
}
