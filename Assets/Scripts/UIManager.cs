﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    public FieldView fieldView;
    public MenuView menuView;
    public GameView gameView;
    public EndGameView endGameView;
}
